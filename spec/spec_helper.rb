require 'puppetlabs_spec_helper/module_spec_helper'

#'Puppet Test Coverage Report'
at_exit {
  puts 'Puppet Test Coverage Report'
  RSpec::Puppet::Coverage.report!
}
