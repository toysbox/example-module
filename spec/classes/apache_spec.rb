require 'puppetlabs_spec_helper/module_spec_helper'

describe 'apache', :type => :class do

  context 'The following class should compile' do
    it { should compile }
  end

  context 'The catalog should contain one package' do
    it { is_expected.to contain_package("httpd").with(
      'ensure' => 'installed'
      )
    }
  end

  context 'The catalog should contain a file named apache_config' do
    it { is_expected.to contain_file("apache_config").with(
      'ensure' => 'file',
      'owner'  => 'root',
      'group'  => 'root',
      'mode'   => '0664',
      )
    }
  end

  context 'The catalog should contain a file named site_index' do
    it { is_expected.to contain_file("site_index").with(
      'ensure' => 'file',
      'owner'  => 'root',
      'group'  => 'root',
      'mode'   => '0664',
      )
    }
  end

  context 'The catalog should contain a service named httpd' do
    it { is_expected.to contain_service("httpd").with(
      'ensure'    => 'running',
      'enable'    => 'true'
      )
    }
  end

end
