require 'spec_helper'

describe 'apache::php::mysql', :type => :class do

  context 'The following class should compile' do
    it { should compile }
  end

  context 'The catalog should contain a package named php-mysql' do
    it { is_expected.to contain_package("php-mysql").with(
      'ensure' => 'installed'
      )
    }
  end

end
