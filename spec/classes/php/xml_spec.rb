require 'spec_helper'

describe 'apache::php::xml', :type => :class do

  context 'The following class should compile' do
    it { should compile }
  end

  context 'The catalog should contain a package named php-xml' do
    it { is_expected.to contain_package("php-xml").with(
      'ensure' => 'installed'
      )
    }
  end

end
