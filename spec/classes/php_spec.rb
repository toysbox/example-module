require 'spec_helper'

describe 'apache::php', :type => :class do

  context 'The following class should compile' do
    it { should compile }
  end

  context 'The catalog should contain a package named php' do
    it { is_expected.to contain_package("php").with(
      'ensure' => 'installed'
      )
    }
  end
end
