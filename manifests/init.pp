class apache {

  package { 'httpd':
    ensure => installed,
  }

  file { 'apache_config':
    ensure => file,
    path   => '/etc/httpd/conf/httpd.conf',
    owner  => 'root',
    group  => 'root',
    mode   => '0664',
    source => 'puppet:///modules/apache/httpd.conf',
    notify => Service['httpd'],
  }

  file { 'site_index':
    ensure  => file,
    path    => '/var/www/html/index.html',
    owner   => 'root',
    group   => 'root',
    mode    => '0664',
    content => epp('apache/index.html.epp'),
  }

  service { 'httpd':
    ensure => running,
    enable => true,
  }

}
