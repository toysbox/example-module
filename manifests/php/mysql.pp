class apache::php::mysql {
  package { 'php-mysql':
   ensure => installed,
  }
}
