class apache::php::xml {
  package { 'php-xml':
    ensure => installed,
  }
}
