# 1. Módulo

Crie a estrutura do módulo

    cd /etc/puppetlabs/code/environments/production/modules
    puppet module generate meetup-apache

# 2. Rspec-puppet

## 2.1 Requisitos

Instale as gems abaixo

    /opt/puppetlabs/puppet/bin/gem install rspec-puppet
    /opt/puppetlabs/puppet/bin/gem install puppetlabs_spec_helper
    /opt/puppetlabs/puppet/bin/gem install puppet-lint
    /opt/puppetlabs/puppet/bin/gem install metadata-json-lint

## 2.2 Estrutura e ajustes

Inicie a estrutura de testes

    cd modules/apache/
    /opt/puppetlabs/puppet/bin/rspec-puppet-init

Escreva seus testes em spec/classes e rode o spec teste com o comando abaixo

## 2.3 Crie os testes

exemplo

```
require 'puppetlabs_spec_helper/module_spec_helper'

describe 'apache', :type => :class do

  context 'The following class should compile' do
    it { should compile }
  end

  context 'The catalog should contain one package' do
    it { is_expected.to contain_package("httpd").with(
      'ensure' => 'installed'
      )
    }
  end

  context 'The catalog should contain a file named apache_config' do
    it { is_expected.to contain_file("apache_config").with(
      'ensure' => 'file',
      'owner'  => 'root',
      'group'  => 'root',
      'mode'   => '0664',
      )
    }
  end

  context 'The catalog should contain a file named site_index' do
    it { is_expected.to contain_file("site_index").with(
      'ensure' => 'file',
      'owner'  => 'root',
      'group'  => 'root',
      'mode'   => '0664',
      )
    }
  end

  context 'The catalog should contain a service named httpd' do
    it { is_expected.to contain_service("httpd").with(
      'ensure'    => 'running',
      'enable'    => 'true'
      )
    }
  end

end

```

## 2.4 Execute os testes

    /opt/puppetlabs/puppet/bin/rake spec

Saída esperada com os testes deste módulo

```
/opt/puppetlabs/puppet/bin/ruby [...] (apresenta comandos e parâmetros conforme SO)
...........

Finished in 2.63 seconds (files took 1.49 seconds to load)
11 examples, 0 failures
```

Crie o arquivo .rspec para melhorar a saída com o conteúdo abaixo

```
--color
--format documentation
```

Saída esperada com os testes deste examplo

```
/opt/puppetlabs/puppet/bin/ruby [...] (apresenta comandos e parâmetros conforme SO)

apache
  The following class should compile
    should compile into a catalogue without dependency cycles
  The catalog should contain one package
    should contain Package[httpd] with ensure => "installed"
  The catalog should contain a file named apache_config
    should contain File[apache_config] with ensure => "file", owner => "root", group => "root" and mode => "0664"
  The catalog should contain a file named site_index
    should contain File[site_index] with ensure => "file", owner => "root", group => "root" and mode => "0664"
  The catalog should contain a service named httpd
    should contain Service[httpd] with ensure => "running" and enable => "true"

apache::php::mysql
  The following class should compile
    should compile into a catalogue without dependency cycles
  The catalog should contain a package named php-mysql
    should contain Package[php-mysql] with ensure => "installed"

apache::php::xml
  The following class should compile
    should compile into a catalogue without dependency cycles
  The catalog should contain a package named php-xml
    should contain Package[php-xml] with ensure => "installed"

apache::php
  The following class should compile
    should compile into a catalogue without dependency cycles
  The catalog should contain a package named php
    should contain Package[php] with ensure => "installed"

Finished in 5.87 seconds (files took 3.27 seconds to load)
11 examples, 0 failures

Puppet Test Coverage Report

Total resources:   11
Touched resources: 7
Resource coverage: 63.64%
Untouched resources:

  Class[Apache::Php::Mysql]
  Class[Apache::Php::Xml]
  Class[Apache::Php]
  Class[Apache]

```

## 2.5 Testes extras

Se quiser rodar o rspec contra um arquivo de testes diretamente

    /opt/puppetlabs/puppet/bin/rspec spec/classes/apache_spec.rb

Se quiser rodar o lint

    /opt/puppetlabs/puppet/bin/rake lint

Se quiser rodar checagem de sintaxe de manifests e templates

    /opt/puppetlabs/puppet/bin/rake syntax

Se quiser rodar checagem de hiera

    /opt/puppetlabs/puppet/bin/rake syntax:hiera

Se quiser rodar checagem de manifests

    /opt/puppetlabs/puppet/bin/rake syntax:manifests

Se quiser rodar checagem de templates ERB

    /opt/puppetlabs/puppet/bin/rake syntax:templates

Se quiser checar tudo

    /opt/puppetlabs/puppet/bin/rake validate

Testando fato (depende de puppet server)

```
[root@centos7 modules]# facter -p|grep httpd
custom_httpd_installed => true
custom_httpd_version => 2.4.6
```

# 3. Serverspec

## 3.1 Requisitos

    /opt/puppetlabs/puppet/bin/gem install serverspec

## 3.2 Setup

    mkdir serverspec
    cd serverspec
    /opt/puppetlabs/puppet/bin/serverspec-init

## 3.3 Crie seu teste

    cd spec/localhost/
    vim httpd_spec.rb

exemplo

```
require 'spec_helper'

describe package('httpd'), :if => os[:family] == 'redhat' do
  it { should be_installed }
end

describe package('php'), :if => os[:family] == 'redhat' do
  it { should be_installed }
end

describe package('php-mysql'), :if => os[:family] == 'redhat' do
  it { should be_installed }
end

describe package('php-xml'), :if => os[:family] == 'redhat' do
  it { should be_installed }
end

describe service('httpd'), :if => os[:family] == 'redhat' do
  it { should be_enabled }
  it { should be_running }
end

describe file('/etc/httpd/conf/httpd.conf') do
  it { should exist }
end

describe file('/var/www/html/index.html') do
  it { should exist }
end

describe file('/etc/httpd/conf/httpd.conf') do
  its(:content) { should match /ServerAdmin root@localhost/ }
end

describe file('/var/www/html/index.html') do
  its(:content) { should match /Hello World/ }
end

describe command('curl http://localhost') do
  its(:stdout) { should match /Oi mundo/ }
end

describe port(80) do
  it { should be_listening }
end
```

### 3.3 Executando

    /opt/puppetlabs/puppet/bin/rake spec

saída esperada com o exemplo acima

```
/opt/puppetlabs/puppet/bin/ruby [...] (apresenta comandos e parâmetros conforme SO)

Package "httpd"
  should be installed

Package "php"
  should be installed

Package "php-mysql"
  should be installed

Package "php-xml"
  should be installed

Service "httpd"
  should be enabled
  should be running

File "/etc/httpd/conf/httpd.conf"
  should exist

File "/var/www/html/index.html"
  should exist

File "/etc/httpd/conf/httpd.conf"
  content
    should match /ServerAdmin root@localhost/

File "/var/www/html/index.html"
  content
    should match /Hello World/

Command "curl http://localhost"
  stdout
    should match /Oi mundo/

Port "80"
  should be listening

Finished in 0.25497 seconds (files took 0.73866 seconds to load)
12 examples, 0 failures
```
