httpd_path = Facter::Core::Execution.exec('which httpd')
httpd_version = Facter::Core::Execution.exec('rpm -q --queryformat "%{VERSION}" httpd')

Facter.add(:custom_httpd_installed) do
  confine :kernel => 'Linux'
  setcode do
    if httpd_path.empty?
      false
    else
      true
    end
  end
end

Facter.add(:custom_httpd_version) do
  confine :custom_httpd_installed => true
  setcode do
   httpd_version
  end
end
