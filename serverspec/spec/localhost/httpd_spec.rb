require 'spec_helper'

describe package('httpd'), :if => os[:family] == 'redhat' do
  it { should be_installed }
end

describe package('php'), :if => os[:family] == 'redhat' do
  it { should be_installed }
end

describe package('php-mysql'), :if => os[:family] == 'redhat' do
  it { should be_installed }
end

describe package('php-xml'), :if => os[:family] == 'redhat' do
  it { should be_installed }
end

describe service('httpd'), :if => os[:family] == 'redhat' do
  it { should be_enabled }
  it { should be_running }
end

describe file('/etc/httpd/conf/httpd.conf') do
  it { should exist }
end

describe file('/var/www/html/index.html') do
  it { should exist }
end

describe file('/etc/httpd/conf/httpd.conf') do
  its(:content) { should match /ServerAdmin root@localhost/ }
end

describe file('/var/www/html/index.html') do
  its(:content) { should match /Hello World/ }
end

describe command('curl http://localhost') do
  its(:stdout) { should match /Oi mundo/ }
end

describe port(80) do
  it { should be_listening }
end
